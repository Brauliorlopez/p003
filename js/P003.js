function Calcular() {

    let txtValor = parseInt(document.getElementById("txtValor").value);
    let txtenganche = parseInt(document.getElementById("txtenganche").value);
    let txtfinanciar = parseInt(document.getElementById("txtfinanciar").value);
    let cmbPlanes = parseInt(document.getElementById("cmbPlanes").value);

    //valor del automovil y enganche
    console.log("Valor del Automovil: " + txtValor);
    let enganche = 30;
    let taenganche = parseFloat((txtValor * enganche) / 100);
    txtPago = document.getElementById("txtPago");
    txtPago.innerHTML = taenganche;
    console.log("Enganche: " + taenganche.toFixed(2));

    //total a financiar
    let totalfianciar = parseFloat(txtValor * 0.30);
    resultado = txtValor - totalfianciar;
    txtfinanciar = document.getElementById("txtfinanciar");
    txtfinanciar.innerHTML = resultado;

    var planes = 0;
    switch (cmbPlanes) {
        case 1: planes = 1.12; break;
        case 2: planes = 1.17; break;
        case 3: planes = 1.21; break;
        case 4: planes = 1.26; break;
        case 5: planes = 1.45; break;
    }
    let tipoplanes = parseFloat(resultado * planes)
    console.log("Total a Financiar: " + tipoplanes.toFixed(2));

    //pago mensual
    var planes = 0;
    switch (cmbPlanes) {
        case 1: planes = 12; break;
        case 2: planes = 18; break;
        case 3: planes = 24; break;
        case 4: planes = 36; break;
        case 5: planes = 48; break;
    }
    let meses = parseFloat(tipoplanes / planes)
    txtPago.innerHTML = txtPago;
    console.log("Pago Mensual: " + meses.toFixed(2));

    //impresion en las cajas de texto
    document.getElementById('txtenganche').value = taenganche.toFixed(2);
    document.getElementById('txtfinanciar').value = tipoplanes.toFixed(2);
    document.getElementById('txtPago').value = meses.toFixed(2);

}

//funcion limpiar cajas de texto y cmb
function Limpiar() {
    document.getElementById("txtValor").value = "";
    document.getElementById("txtenganche").value = "";
    document.getElementById("txtfinanciar").value = "";
    document.getElementById("cmbPlanes").value = "";
    document.getElementById("txtPago").value = "";
}